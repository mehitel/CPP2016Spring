#include <stdio.h>
#include <stdlib.h>

#define max(a,b)    (((a) > (b)) ? (a) : (b))

unsigned int binarySearch(int s[], int low, int high, int key) { // values : sorted array
	int mid = (low + high) / 2;

	/* it means NOT FOUND */
	if (low > high) {
		return -1;
	}

	/* key is found in array */
	if (key == s[mid]) {
		return mid;
	}
	else {
		/* array[low] <= key <= array[mid-1] */
		if (key < s[mid]) {
			return binarySearch(s, low, mid - 1, key);
		}
		/* array[mid+1] <= key <= array[high] */
		else {
			return binarySearch(s, mid + 1, high, key);
		}
	}
}

unsigned int binarySum(int s[], int low, int high) {
	int m = (low + high) / 2;

	/* there is nothing to add */
	if (low > high) {
		return 0;
	}
	else {
		return binarySum(s, low, m - 1) + s[m] + binarySum(s, m + 1, high);
	}
}

unsigned int binaryMax(int s[], int low, int high) {
	int m = (low + high) / 2;
	int maxval = 0;
	int leftSide, rightSide;

	/* there is nothing to compare */
	if (low > high) {
		return 0;
	}
	else {
		/* find max in between array[low] and array[m-1] */
		leftSide = binaryMax(s, low, m - 1);
		/* find max in between array[m+1] and array[high] */
		rightSide = binaryMax(s, m + 1, high);

		maxval = max(leftSide, rightSide);
		maxval = max(s[m], maxval);

		return maxval;
	}
}

void inputScore(int s[], int n) {
	FILE *fp;
	int i;
	
	fp = fopen("math.dat", "r");
	for (i = 0; i < n; i++) {
		/* read one of number */
		if (fscanf(fp, "%d", &s[i]) == EOF) {
			/* end of file */
			break;
		}
	}
}

int main() {
	int n = 10;
	int *score;
	int key = 8;
	unsigned int search;
	unsigned int sum, max;

	/* input score data from math.dat */
	score = (int *)malloc(sizeof(int) * n);
	inputScore(score, n);

	/* find key in array by using binarySearch */
	search = binarySearch(score, 0, n - 1, key);
	if (search == -1) {
		printf("NOT FOUND\n");
	}
	else {
		printf("%u\n", search);
	}

	/* print total sum of array by using binarySum */
	printf("BinarySum low : %d, high : %d\n", 0, n - 1);
	sum = binarySum(score, 0, n - 1);
	printf("sum : %d\n", sum);

	/* print max of array by using binaryMax */
	printf("BinaryMax low : %d, high : %d\n", 0, n - 1);
	max = binaryMax(score, 0, n - 1);
	printf("max : %d\n", max);

	free(score);
	return EXIT_SUCCESS;
}